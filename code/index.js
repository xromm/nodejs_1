class User {
  constructor(name, email) {
    this.email = email;
    this.name = name;
  }
  logInfo(){
    console.log(`name|email = ${this.name}|${this.email}`);
  }
}

let user = new User("nikit", "xpom66@mail.ru");
user.logInfo();

// myConfig
import config from './config/index'

// mongodb
import Mongo from './server/serverdb'
var db = new Mongo(dbReady);

// express
var express = require('express');
var app = express();

// body-pareser
var bodyParser = require('body-parser');

// React
require('babel-register')({presets: ['react']});
import React from "react";
var ReactDOMServer = require('react-dom/server');
import Table from './views/View/View';

// react hot module replacement
var path = require('path');
var webpack = require('webpack');
var webpackConfig = require('../webpack.config.js');

import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

// inicial hmre
var webpackCompiler = webpack(webpackConfig);

app.use(webpackDevMiddleware(webpackCompiler, {noInfo: true, publicPath: webpackConfig.output.publicPath}))
app.use(webpackHotMiddleware(webpackCompiler));

// db ready
function dbReady() {
	// // delete all data
	// db.deleteFromDB( { },
	//   function() {  }
	// );
	//
	// var dataLineNumer = 1;
	//
	// for (var i = 0; i < dataLineNumer; i++) {
	//   db.addData(
	//     { myName: "Nikita",
	//     mySurname:"Chernyshov number NEWWW 123123",
	//     age: 10 + i},
	//     function() { }
	//   );
	// }
	//
	// for (var i = 0; i < dataLineNumer; i++) {
	//   db.addData(
	//     { myName: "Nikita 2",
	//       mySurname:"Chernyshov number NEWWW 123123",
	//       age: 20 + i},
	//     function() {}
	//   );
	//}

	// setTimeout(function () {
	//   db.find(  { $or: [{ myName: { $eq: "Nikita"} }, { myName: { $eq: "Nikita 2"} }] },
	//   function(res) {
	//     data = res;
	//     console.log("Data from DB = " + data);
	//     for(var i = 0; i < data.length; i++) {
	//
	//       console.log(data[i].myName);
	//     }
	//   } );
	//
	//   //db.close();
	// }, 100);
}

// body-parser
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({extended: true})); // for parsing application/x-www-form-urlencoded

// server
app.use(express.static('public'));

var PORT = config.get('port');
app.listen(PORT, function() {
	console.log("http://localhost:" + PORT);
});

// Middleware
app.use(function mainPage(req, res, next) {
	if (req.url == "/") {

    // db.find({
    //   $or: [
    //     {
    //       myName: {
    //         $eq: "Nikita"
    //       }
    //     }, {
    //       myName: {
    //         $eq: "Nikita 2"
    //       }
    //     }
    //   ]
    // }, function(data) {
    //
    //   for (var i = 0; i < data.length; i++) {
    //
    //     console.log(data[i].myName);
    //   }
    //
		// 	var props = {
		// 		title: "data: data for main table",
		// 		data: data
		// 	};
    //
		// 	var html = ReactDOMServer.renderToString(React.createElement(Table, props));
		// 	res.send(html);
		// });

	} else {
		next();
	}
});

app.use(function addDataToDBPage(req, res, next) {
	if (req.url == "/add") {

		// db.find({}, function(data) {
    //
		// 	for (var i = 0; i < data.length; i++) {
    //
		// 		console.log(data[i].myName);
		// 	}
    //
		// 	var props = {
		// 		title: "add data to db",
		// 		data: data
		// 	};
    //
		// 	var html = ReactDOMServer.renderToString(React.createElement(Table, props));
		// 	res.send(html);
		// });

	} else {
		next();
	}
});

app.use(function addDataToDBPage(req, res, next) {
	if (req.url == "/db/addNewRecord") {

		console.log("Add new record");
		//console.log(req);
		console.log(req.method);
		console.log(req.hostname);
		console.log(req.body);

		var newRecord = JSON.stringify(req.body);

		console.log("newRecord = " + newRecord);

		for (var i = 0; i < req.body.submitData.length; i++) {
			var item = req.body.submitData[i];
			console.log(item.num + " | " + item.name + " | " + item.value);
		}
    //
		// db.addData({
		// 	[req.body.submitData[0].name]: req.body.submitData[0].value,
		// 	[req.body.submitData[1].name]: req.body.submitData[1].value,
		// 	[req.body.submitData[2].name]: req.body.submitData[2].value,
		// 	[req.body.submitData[3].name]: req.body.submitData[3].value
		// }, function(err) {
    //   if (err) throw err;
    //   console.log(" DB = new record was added");
    // });

	} else {
		next();
	}
});

app.use(function notFoundPage(req, res) {
	res.status(404).send("Извините, страница не найдена.");
})


//////////////////////////////////////////////////////////////////////////////

// shutdown process Ctrl-C
process.on( 'SIGINT', function() {
  console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
  // some other closing procedures go here
  process.exit( );
	process.kill(process.pid, 'SIGINT');
})

// process.once('SIGINT', function () {
//   gracefulShutdown(function () {
//     process.kill(process.pid, 'SIGINT'); // SIGUSR2
//   });
// });
