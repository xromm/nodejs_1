var React = require('react');

var InputHandlerForSubmission = require('./InputHandlerForSubmission');

var SubmitNewItemForm = React.createClass({

	getInitialState: function() {
		var data = this.props.data;
		return {data};
	},

	handleCommentSubmit: function(submitData) {
		var self;
		self = this;

		// Submit form via jQuery/AJAX
		$.ajax({type: 'POST', url: '/db/addNewRecord', data:
				{submitData}
			}).done(function(data) {
			self.clearForm()
		}).fail(function(jqXhr) {
			console.log('failed to register');
		});
	},

	render: function() {
		return (
			<div className="commentBox">
				<h1>Comment Form</h1>
				<InputHandlerForSubmission data={this.state.data} onCommentSubmit={this.handleCommentSubmit}/>
			</div>
		);
	}
});

export default SubmitNewItemForm
