import React from 'react'

var InputHandlerForSubmission = React.createClass({

	getInitialState: function() {
		var data = this.props.data;

		// massive for names from data
		var index = [];

		// read all names
		for (var x in data) {
			index.push(x);
		};

		return {index};
	},
	_addNewRecord: function(e) {
		e.preventDefault();
		console.log("add new records");

		var array = this.state;

		var newInput = [];
		var index = this.state.index;

		for (var i = 0; i < index.length; i++) {
			console.log(i + " index : " + index[i] + " | " + array[index[i]]);

			var name = index[i];
			var value = array[index[i]];

			var obj = {};
			obj['num'] = i;
			obj['name'] = name;
			obj['value'] = value;

			newInput.push( obj );
		}

    var testVal = JSON.stringify(newInput);
    console.log(testVal);
		this.props.onCommentSubmit( newInput );
	},

	_inputChanged: function(i, name, event) {

		this.setState({
			[name]: event.target.value
		}, function logNewSetState() {
			console.log(" CallBack " + i + "№ input '" + name + "' | SetState = " + this.state[name]);
		});

		console.log(i + "№ input '" + name + "' add new records : " + event.target.value + " | SetState = " + this.state[name]);
	},

	render: function() {
		var index = this.state.index;
		return (
			<form onSubmit={this._addNewRecord.bind(this)}>

				{index.map(function(res, i) {
					var boundChange = this._inputChanged.bind(this, i, res);

					return <input placeholder={res} key={i + " input"} onChange={boundChange}/>
				}, this)}

				<input type="submit" value="Add"/>

			</form>
		);
	}
});

export default InputHandlerForSubmission;
