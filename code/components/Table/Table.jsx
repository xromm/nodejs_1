var React = require('react');
var Griddle = require('griddle-react');

// material ui

var React = require('react');
var PropTypes = React.PropTypes;

var SubmitForm = React.createClass({

	getInitialState: function() {
		var data = this.props.data;

		// massive for names from data
		var index = [];

		// read all names
		for (var x in data) {
			index.push(x);
		};

		return {index};
	},
	_addNewRecord: function(e) {
		e.preventDefault();

		console.log("add new records");

		var array = this.state;

		var newInput = [];
		var index = this.state.index;

		for (var i = 0; i < index.length; i++) {
			console.log(i + " index : " + index[i] + " | " + array[index[i]]);

			var name = index[i];
			var value = array[index[i]];

			var obj = {};
			obj['num'] = i;
			obj['name'] = name;
			obj['value'] = value;

			newInput.push( obj );
		}

    var testVal = JSON.stringify(newInput);

    console.log(testVal);

		this.props.onCommentSubmit( newInput );

		// null massive after sending data
		//newInput = [];
	},
	_inputChanged: function(i, name, event) {

		this.setState({
			[name]: event.target.value
		}, function logNewSetState() {
			console.log(" CallBack " + i + "№ input '" + name + "' | SetState = " + this.state[name]);
		});

		console.log(i + "№ input '" + name + "' add new records : " + event.target.value + " | SetState = " + this.state[name]);
	},
	render: function() {
		var index = this.state.index;
		return (
			<form onSubmit={this._addNewRecord}>

				{index.map(function(res, i) {
					var boundChange = this._inputChanged.bind(this, i, res);

					return <input placeholder={res} key={i + " input"} onChange={boundChange}/>
				}, this)}

				<input type="submit" value="Добавить запись"/>

			</form>
		);
	}
});

var MyInput = React.createClass({

	getInitialState: function() {
		var data = this.props.data;

		return {data};
	},
	handleCommentSubmit: function(submitData) {

		var self;
		self = this;

		// Submit form via jQuery/AJAX
		$.ajax({type: 'POST', url: '/db/addNewRecord', data:
				{submitData}
			}).done(function(data) {
			self.clearForm()
		}).fail(function(jqXhr) {
			console.log('failed to register');
		});

	},
	render: function() {
		return (
			<div className="commentBox">
				<h1>Comment Form</h1>
				<SubmitForm data={this.state.data} onCommentSubmit={this.handleCommentSubmit}/>
			</div>
		);
	}

});

module.exports = React.createClass({
	_handleClick: function() {

		//alert("hello " + this.state.styleBtn.display);
		if (this.state.styleBtn.display == 'none') {
			this.setState({
				styleBtn: {
					'display': 'block'
				}
			});
		} else {
			this.setState({
				styleBtn: {
					'display': 'none'
				}
			});
		}
	},

	getInitialState: function() {
		var styleBtn = {
			display: 'none'
		};
		return {styleBtn};
	},

	render: function() {
		return (
			<html>
				<head>
					<title>{this.props.title}</title>

					<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js"/>

					<link rel='stylesheet' href="/style.css"/>
				</head>
				<body>
					<h1>{"TABLE PAGE " + this.props.title}</h1>
					<button onClick={this._handleClick}>{"CLICK " + this.state.styleBtn.display}</button>
					<button onClick={this._handleClick} style={this.state.styleBtn}>CLICK2</button>

					<Griddle results={this.props.data} columns={["myName", "age"]} showFilter={true} showSettings={true}/>

					<MyInput data={this.props.data[0]}/>

					<script dangerouslySetInnerHTML={{
						__html: 'window.PROPS=' + JSON.stringify(this.props)
					}}/>
					<script src='/bundle.js'/>
				</body>
			</html>
		);
	}

});
