var webpack = require('webpack');
var path = require('path');

module.exports = {
	debtool: 'inline-source-map',
	entry: [
		'webpack-hot-middleware/client',
		'./dist/views/client.js'
	],
	output: {
		path: path.resolve("./build"),
		filename: 'bundle.js',
		publicPath:'/'
	},
	module: {
		loaders: [{
			test: /\.jsx?$/,
			exclude: [
				/node_modules/,
				/code/
			],
      include: [
				'./dist/components',
				'./dist/views'
			],
			loader: 'babel-loader',
			query: {
				optional: ['runtime'],
        plugins: ['transform-runtime'],
				presets: [ 'react', 'es2015', 'react-hmre']
			}
		}]
	},
	plugins: [
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin()
	]

}
